<?php include "templates/include/header.php" ?>
        
        <!-- site-main -->
        <div id="main" class="site-main">
		
        
		
			
        
       		<!-- post-slider ->
            <div class="post-slider owl-carousel" data-items="3" data-loop="true" data-center="true" data-mouse-drag="true" data-nav="true" data-dots="false" data-autoplay="false" data-autoplay-speed="600" data-autoplay-timeout="2000">
                
                <!-- post ->
                <div class="post-thumbnail" style="background-image:url(images/blog/01.jpg)">
                        
                    <!-- .entry-header ->
                    <header class="entry-header">
                        
                        <!-- .entry-meta ->
                        <div class="entry-meta">
                            <span class="cat-links">
                                <a href="#" title="View all posts in Life" rel="category tag">Life</a>
                            </span>	
                        </div>
                        <!-- .entry-meta ->
                        
                        <!-- .entry-title ->
                        <h2 class="entry-title"><a href="blog-single.html">Embracing Minimalist Lifestyle</a></h2>
                        
                        <p><a href="blog-single.html" class="more-link">View Post</a></p>
                        
                    </header>
                    <!-- .entry-header ->
                    
                </div>
                <!-- post ->    
                
                <!-- post ->
                <div class="post-thumbnail" style="background-image:url(images/blog/02.jpg)">
                        
                    <!-- .entry-header ->
                    <header class="entry-header">
                    
                        <!-- .entry-meta ->
                        <div class="entry-meta">
                            <span class="cat-links">
                                <a href="#" title="View all posts in Travel" rel="category tag">Travel</a>
                            </span>	
                        </div>
                        <!-- .entry-meta ->
                        
                        <!-- .entry-title ->
                        <h2 class="entry-title"><a href="blog-single.html">My Life as a Digital Nomad</a></h2>
                        
                        <p><a href="blog-single.html" class="more-link">View Post</a></p>
                        
                    </header>
                    <!-- .entry-header ->
                    
                </div>
                <!-- post ->  
                
                
                <!-- post ->
                <div class="post-thumbnail" style="background-image:url(images/blog/03.jpg)">
                        
                    <!-- .entry-header ->
                    <header class="entry-header">
                        
                        <!-- .entry-meta ->
                        <div class="entry-meta">
                            <span class="cat-links">
                                <a href="#" title="View all posts in Travel" rel="category tag">Stuff</a>
                            </span>	
                        </div>
                        <!-- .entry-meta -->
                        
                        <!-- .entry-title ->
                        <h2 class="entry-title"><a href="blog-single.html">Mastering Portrait Photography</a></h2>
                        
                        <p><a href="blog-single.html" class="more-link">View Post</a></p>
                        
                    </header>
                    <!-- .entry-header ->
                    
                </div>
                <!-- post ->  
                
                <!-- post ->
                <div class="post-thumbnail" style="background-image:url(images/blog/04.jpg)">
                        
                    <!-- .entry-header ->
                    <header class="entry-header">


                        
                        <!-- .entry-meta ->
                        <div class="entry-meta">
                            <span class="cat-links">
                                <a href="#" title="View all posts in Travel" rel="category tag">Travel</a>
                            </span>	
                        </div>
                        <!-- .entry-meta ->
                        
                        <!-- .entry-title ->
                        <h2 class="entry-title"><a href="blog-single.html">Having Fun With Polar Bears</a></h2>
                        
                        <p><a href="blog-single.html" class="more-link">View Post</a></p>
                        
                    </header>
                    <!-- .entry-header ->
                    
                </div>
                <!-- post ->  
                
                <!-- post ->
                <div class="post-thumbnail" style="background-image:url(images/blog/05.jpg)">
                        
                    <!-- .entry-header ->
                    <header class="entry-header">
                        
                        <!-- .entry-meta ->
                        <div class="entry-meta">
                            <span class="cat-links">
                                <a href="#" title="View all posts in Travel" rel="category tag">Tips</a>
                            </span>	
                        </div>
                        <!-- .entry-meta ->
                        
                        <!-- .entry-title ->
                        <h2 class="entry-title"><a href="blog-single.html">How To Write Kick-ass Articles</a></h2>
                        
                        <p><a href="blog-single.html" class="more-link">View Post</a></p>
                        
                    </header>
                    <!-- .entry-header ->
                    
                </div>
                <!-- post ->  
                    
            </div>
            <!-- post-slider -->
		
        
			
			
			
			
            
			
			<div class="layout-medium"> 
                <div id="primary" class="content-area">
                    <!-- site-content -->
                    <div id="content" class="site-content" role="main">
                    
                        <!-- entry-header -->
                        <!--<header class="entry-header">
                        	<h1 class="entry-title">Latest Stories</h1>
                        </header>-->
                        <!-- entry-header -->
                        
                        
                       
                        
                
                
                        <!-- BLOG LIST -->
                        <div class="blog-list  blog-stream">
                        
                
                        
                        <?php foreach ( $results['articles'] as $article ) { ?>
                        
                        	<!-- .hentry -->
                            <article class="hentry post has-post-thumbnail">
                                <!-- .featured-image -->
                                <div class="featured-image">
                                    <a href=".?action=viewArticle&amp;articleId=<?php echo $article->id?>"><img src="<?php echo $article->img?>" alt="blog-image"></a>
                                </div>
                                <!-- .featured-image -->
                                
                                <!-- .hentry-middle -->
                                <div class="hentry-middle">
                                    <!-- .entry-header -->
                                    <header class="entry-header">
                                        <!-- .entry-meta -->
                                        <div class="entry-meta">
                                            <span class="cat-links">
                                                <a href=".?action=viewArticle&amp;articleId=<?php echo $article->id?>" title="" rel="category tag"><?php echo $article->category?></a>
                                            </span>	
                                        </div>
                                        <!-- .entry-meta -->
                                        <!-- .entry-title -->
                                        <h2 class="entry-title"><a href=".?action=viewArticle&amp;articleId=<?php echo $article->id?>"><?php echo htmlspecialchars( $article->title )?></a></h2>
                                    </header>
                                    <!-- .entry-header -->
                                    
                                    <!-- .entry-content -->
                                    <div class="entry-content">        
                                        <p><?php echo $article->summary?>
                                            <span class="more">
                                                <a href=".?action=viewArticle&amp;articleId=<?php echo $article->id?>" class="more-link">Mehr</a>
                                            </span>
                                        </p>
                                    </div>
                                    <!-- .entry-content -->
                                </div>
                                <!-- .hentry-middle -->
                            </article>
                            <!-- .hentry -->
                            <?php } ?>

                            
                       </div>
                       <!-- BLOG LIST -->
                        
                        
                        
                    </div>
                    <!-- site-content -->
            
                </div>
                <!-- primary -->    
            
            
            	
            
            
            </div>
            <!-- layout -->
        
        
        </div>
        <!-- site-main -->
        
<?php include "templates/include/footer.php" ?>