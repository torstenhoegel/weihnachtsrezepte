<?php include "templates/include/header.php" ?>
        <!-- site-main -->
        <div id="main" class="site-main">
<div class="layout-full"> 
            <div id="primary" class="content-area">
             
        
            

            
                    <!-- site-content -->
                    <div id="content" class="site-content" role="main"> <!-- .hentry -->
                        <article class="hentry page">
                        
                            
                            <!-- .entry-header -->   
                            <header class="entry-header">
                                <h1 class="entry-title"><?php echo $results['seiten']->name?></h1>
                            </header>
                            <!-- .entry-header -->   
                            
                            
                            <!-- .entry-content -->
                            <div class="entry-content">
                                
                                <?php echo $results['seiten']->inhalt?>
                                
                                
                            </div>
                            <!-- .entry-content -->
                            
                            
                        </article>
                        <!-- .hentry -->
                        
                    </div>
                    <!-- site-content -->
            
            </div>
                <!-- primary -->    
            
            
                
            
            
            </div>
            <!-- layout -->
        
        
        </div>
        <!-- site-main -->
        
<?php include "templates/include/footer.php" ?>