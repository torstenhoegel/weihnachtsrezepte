       
<?php include "templates/include/header.php" ?>

        <!-- site-main -->
        <div id="main" class="site-main">
			<div class="layout-medium"> 
            	<div id="primary" class="content-area">
             
            
                    <!-- site-content -->
                    <div id="content" class="site-content" role="main"> <!-- .hentry -->
                        <article class="hentry page">
                            
                                
                            <!-- .entry-content -->
                            <div class="entry-content intro" data-animation="rotate-1">
                                
                                
                                
                                <!-- .profile-image -->
                                <div class="profile-image">
                                	<img alt="profile" src="/assets/images/site/xmas_tree.png"/>
                                </div>  
                                <!-- .profile-image -->
                                
                                
                                <h2><em>XMas</em> Weihnachtslieder</h2>
                                <h3>Rund um <strong>Pop</strong> <strong>Jazz</strong> <strong>Oldies</strong></h3>
                                <h3><p id='countdown'></p></h3>
                                
                                
                                <!-- .link-boxes -->
                                <!-- <figure>
                                	<a href="/?action=category&category=pop"><img src="images/site/box-01.jpg" alt="POP"></a>
                                    <figcaption class="wp-caption-text">POP</figcaption>
                                </figure>        
                                
                                <figure>
                                	<a href="/?action=category&category=jazz"><img src="images/site/box-02.jpg" alt="JAZZ"></a>
                                    <figcaption class="wp-caption-text">JAZZ</figcaption>
                                </figure> 
                                
                                <figure>
                                	<a href="/?action=category&category=oldies"><img src="images/site/box-03.jpg" alt="OLDIES"></a>
                                    <figcaption class="wp-caption-text">Oldies</figcaption>
                                </figure> 
                                
                                <figure>
                                	<a href="/?action=category&category=instr"><img src="images/site/box-04.jpg" alt="INSTR"></a>
                                    <figcaption class="wp-caption-text">Instrumental</figcaption>
                                </figure> 

 -->
                                <?php foreach ( $results['categories'] as $category ) { ?>
                                <figure>
                                	<a href="/kategorie/<?php echo $category->id?>"><img src="<?php echo $category->img?>" alt="KATEGORIE"></a>
                                    <figcaption class="wp-caption-text"><?php echo htmlspecialchars( $category->name )?></figcaption>
                                </figure> 
                                <?php } ?>

                                <!-- .link-boxes -->         
                                	
                                    
                             </div> 
                             <!-- .entry-content -->
                                
                                	
                                    
                         </article> 
                         <!-- .page -->
                                 
                                 
                                 
                                 
                       <!-- .home-title -->
                       <h3 class="widget-title home-title">Neuste Lieder</h3>  
                         
                        
                       <!-- BLOG SIMPLE -->
                       <div class="blog-simple">
                            



						<?php foreach ( $results['articles'] as $article ) { ?>
                        
                            <!-- .hentry -->
                            <article class="hentry post has-post-thumbnail">
                                
                                <!-- .hentry-left -->
                                <div class="hentry-left">
                                    <div class="entry-date">
                                        <span class="day"><?php echo htmlspecialchars( $article->category )?></span>
                                    </div>
                                    <div class="featured-image" style="background-image:url(<?php echo $article->img?>)"></div>
                                </div>
                                <!-- .hentry-left -->
                                
                                <!-- .hentry-middle -->
                                <div class="hentry-middle">
                                        
                                    <!-- .entry-title -->
                                    <h2 class="entry-title"><a href="/lied/<?php echo $article->id?>"><?php echo htmlspecialchars( $article->title )?></a></h2>
                            
                                </div>
                                <!-- .hentry-middle -->
                                
                                <a class="post-link" href="/lied/<?php echo $article->id?>"><?php echo htmlspecialchars( $article->title )?></a>
                                
                            </article>
                            <!-- .hentry -->
                            
                         <?php } ?>



                            
                       </div> 
                       <!-- BLOG SIMPLE -->
                               
                                
                    </div>
                    <!-- site-content -->
            
            </div>
                <!-- primary -->    
            
            
            	
            
            
            </div>
            <!-- layout -->
        
        
        </div>
        <!-- site-main -->


<script src="/assets/js/snow.js"></script>
        <?php include "templates/include/footer.php" ?>
