<!DOCTYPE html>

<html lang="en" class="minimal-style is-menu-fixed is-always-fixed is-selection-shareable blog-animated header-light header-small" data-effect="slideUp">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="XMAS - Christmas Songs">
    <meta name="keywords" content="school, schüler, projekt, xmas, christmas, songs">
    <meta name="author" content="Go-On Network">
    <title><?php echo htmlspecialchars( $results['pageTitle'] )?></title>
    
    <!-- FAV and TOUCH ICONS -->
    <link rel="shortcut icon" href="/assets/images/ico/favicon.ico">
    <link rel="apple-touch-icon" href="/assets/images/ico/apple-touch-icon.png"/>
    
    <!-- FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700|Noto+Sans:400,400i,700,700i|Poppins:300,400,500,600,700" rel="stylesheet">
    
    <!-- STYLES -->
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/fonts/fontello/css/fontello.css">
    <link rel="stylesheet" type="text/css" href="/assets/js/jquery.magnific-popup/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="/assets/js/jquery.fluidbox/fluidbox.css">
    <link rel="stylesheet" type="text/css" href="/assets/js/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="/assets/js/selection-sharer/selection-sharer.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/rotate-words.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/align.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/main.css">
    <link rel="stylesheet" type="text/css" href="/assets/js/shortcodes/shortcodes.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/768.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/992.css">

    <!-- INITIAL SCRIPTS -->
	<script src="/assets/js/modernizr.min.js"></script>

	<script src="https://cdn.tiny.cloud/1/35788zy6v716jgochbe0mcm6mnda8jqxf87xhd0uzdwclm5t/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

    <!-- PWA -->
    <link rel="apple-mobile-web-app-title" href="XMas" />
    <link rel="manifest" href="./manifest.json" type="application/json" >

    <!-- place this in a head section -->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <link href="/assets/images/apple/2048x2048.png" sizes="2048x2732" rel="apple-touch-startup-image" />
    <link href="/assets/images/apple/1668x2224.png" sizes="1668x2224" rel="apple-touch-startup-image" />
    <link href="/assets/images/apple/1536x2048.png" sizes="1536x2048" rel="apple-touch-startup-image" />
    <link href="/assets/images/apple/1125x2436.png" sizes="1125x2436" rel="apple-touch-startup-image" />
    <link href="/assets/images/apple/1242x2208.png" sizes="1242x2208" rel="apple-touch-startup-image" />
    <link href="/assets/images/apple/750x1334.png" sizes="750x1334" rel="apple-touch-startup-image" />
    <link href="/assets/images/apple/640x1136.png" sizes="640x1136" rel="apple-touch-startup-image" />

    <script>
    // Detects if device is on iOS 
    const isIos = () => {
      const userAgent = window.navigator.userAgent.toLowerCase();
      return /iphone|ipad|ipod/.test( userAgent );
    }
    // Detects if device is in standalone mode
    const isInStandaloneMode = () => ('standalone' in window.navigator) && (window.navigator.standalone);

    // Checks if should display install popup notification:
    if (isIos() && !isInStandaloneMode()) {
      this.setState({ showInstallMessage: true });
    }
    </script>
 	
</head>

<body class="  ">

    <!-- page -->
    <div id="page" class="hfeed site">
        
        <!-- header -->
        <header id="masthead" class="site-header" role="banner">
 			
            
            <!-- site-navigation -->
            <nav id="primary-navigation" class="site-navigation primary-navigation" role="navigation">
                
                
                
                <!-- layout-medium -->
                <div class="layout-medium">
                
                    
                    
                    
                    <!-- site-title : image-logo -->
                    <h1 class="site-title">
                        <a href="/" rel="home">
                            <img src="/assets/images/site/logo.png" alt="logo">
                        	<span class="screen-reader-text">XMas</span>

                        </a>
                    </h1>
                    <!-- site-title -->
                    
                    <!-- site-title : text-logo -->
                    <!--<h1 class="site-title">
                        <a href="..//" rel="home">
                            Haley Dust
                        </a>
                    </h1>-->
                    <!-- site-title -->
                    
                    
                    
                
                    <a class="menu-toggle"><span class="lines"></span></a>
                    
                    <!-- nav-menu -->
                    <div class="nav-menu">
                        <ul>
                            <li><a href="/">HOME</a></li>

                            <li><a href="#">KATEGORIEN</a>
                                <!-- level 2 -->
                                <ul><!-- 
                                    <li><a href="/?action=category&category=pop">Pop</a></li>
                                    <li><a href="/?action=category&category=charts">Charts</a></li>
                                    <li><a href="/?action=category&category=klassik">Klassik</a></li>
                                    <li><a href="/?action=category&category=jazz">Jazz</a>
                                    <li><a href="/?action=category&category=oldies">Oldies</a>
                                    <li><a href="/?action=category&category=chor">Chor</a>
                                    <li><a href="/?action=category&category=instr">Instrumental</a>

 -->
                                    <?php foreach ( $results['categories'] as $category ) { ?>
                                    <li><a href="/kategorie/<?php echo $category->id?>"><?php echo htmlspecialchars( $category->name )?></a></li>
                                    <?php } ?>


                                </ul>
                                <!-- level 2 -->

                            </li>
                            <!-- <li><a href="#">SEITEN</a></li> -->

                        </ul>
                    </div>
                    <!-- nav-menu -->
                    
                    <!-- social-container -->
                    <div class="social-container">
                        
                        <a href="/admin.php">ADMINISTRATION</a>

    
                    </div>
                    <!-- social-container -->
        
                </div>
            </nav>
            <!-- site-navigation -->



            
            		
        </header>
        <!-- header -->
        
