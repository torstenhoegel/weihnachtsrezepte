<!-- site-footer -->
        <footer id="colophon" class="site-footer" role="contentinfo">
			
            <!-- layout-medium -->
            <div class="layout-medium">
            
            	
                <!-- site-title-wrap -->
                <div class="site-title-wrap">


                            
                        <div class="center">
                            <form action="https://panel.go-on-net.de/lists/ap656z82trf20/subscribe" method="post">
                            <aside class="widget widget_mc4wp_widget">
                                <h3 class="widget-title">Für den Newsletter Registrieren</h3>
                                <div class="form mc4wp-form">
                                    <form method="post">
                                        <p>
                                            <label>Email Adresse: </label>
                                            <input type="email" name="EMAIL" id="EMAIL" placeholder="Deine E-Mail Adresse" required>
                                            <label>Vorname: </label>
                                            <input type="text" name="FNAME" id="FNAME" placeholder="Dein Vorname" required>
                                            <label>Nachname: </label>
                                            <input type="text" name="LNAME" id="LNAME" placeholder="Deine Nachname" required>
                                            <label>Lieblingsgenre: </label>
                                            <select data-title="Lieblingsgenre" data-container="body" data-toggle="popover" data-content="Bitte wählen sie ihr Lieblingsgenre aus!" class="form-control has-help-text field-lgr field-type-dropdown" placeholder="Lieblingsgenre" data-placement="top" name="LGR" id="LGR"><option value="Pop">Pop</option><option value="Kinderlieder">Kinderlieder</option><option value="Oldies">Oldies</option><option value="Klassik">Klassik</option><option value="Jazz">Jazz</option></select>
                                        </p>
                                        <p>
                                            <input type="submit" value="Anmelden">
                                        </p>
                                    </form>
                                </div>
                            </aside>
                             </form>
                        </div><br><br>
       
                
                
                    <!-- site-title : text-logo -->
                    <!--<h1 class="site-title">
                        <a href="..//" rel="home">
                            Jeff Winger
                        </a>
                    </h1>-->
                    <!-- site-title -->
                               
                    <!-- site-title : image-logo -->
                    <h1 class="site-title">
                        <a href="/" rel="home">
                            <img src="/assets/images/site/logo.png" alt="logo">
                        </a>
                    </h1>
                    <!-- site-title -->
                    
                    <p class="site-description">wir kennen deinen nächsten lieblingssong</p>
                
                </div>
                <!-- site-title-wrap -->
                
                        <div class="center">
                            <aside class="widget widget_categories">
                              <ul>
                                <li class="cat-item"><a href="/seite/4" title="View all posts filed under Nature">Nutzungsbedingungen</a></li>
                                <li class="cat-item"><a href="/seite/3" title="View all posts filed under Life">Datenschutz</a></li>
                                <li class="cat-item"><a href="/seite/2" title="View all posts filed under Adventure">Impressum</a></li>
                              </ul>
                            </aside>
                        </div>
            	
                <!-- footer-social -->
                <div class="footer-social">
                	<!-- 
                    <div class="textwidget">
                        <a class="social-link facebook" href="#"></a>
                        <a class="social-link twitter" href="#"></a>
                        <a class="social-link vine" href="#"></a>
                        <a class="social-link dribbble" href="#"></a>
                        <a class="social-link instagram" href="#"></a>
                    </div> -->

                </div>
                <!-- footer-social -->
                
                
                
                <!-- widget-area -->
                <div class="widget-area" role="complementary">
           
                    <div class="row"><!-- 
                        
                        <div class="col-sm-6 col-md-4">
                        	
                            
                            <aside class="widget widget_text">
                              
                              <div class="textwidget">
                                <a href="#"><img src="../images/blog/banner.jpg" alt="banner"></a>
                              </div>
                            </aside>
                        
                        </div> -->
                    	
                        <div class="col-sm-6 col-md-4">
                        	
                            
                        </div>
                        <!-- 
                        <div class="col-sm-12 col-md-4">
                        	
                            <aside class="widget widget_widget_tptn_pop">
                                <h3 class="widget-title">Trending Posts</h3>
                                <div class="tptn_posts tptn_posts_widget">
                                    <ul>
                                    
                                        <li>
                                            <a href="#" class="tptn_link">
                                                <img src="../images/blog/p2.jpg" alt="post-image" class="tptn_thumb">
                                            </a>
                                            <span class="tptn_after_thumb">
                                                <a href="#" class="tptn_link"><span class="tptn_title">Feel The Wind</span></a>
                                                <span class="tptn_date"> September 3, 2014</span> 
                                            </span>
                                        </li>
                                    
                                        <li>
                                            <a href="#" class="tptn_link">
                                                <img src="../images/blog/p3.jpg" alt="post-image" class="tptn_thumb">
                                            </a>
                                            <span class="tptn_after_thumb">
                                                <a href="#" class="tptn_link"><span class="tptn_title">Stop Worrying About How Pretty It is</span></a>
                                                <span class="tptn_date"> September 3, 2014</span> 
                                            </span>
                                        </li>
                                    
                                        <li>
                                            <a href="#" class="tptn_link">
                                                <img src="../images/blog/p4.jpg" alt="post-image" class="tptn_thumb">
                                            </a>
                                            <span class="tptn_after_thumb">
                                                <a href="#" class="tptn_link"><span class="tptn_title">10 Killer Blogging Tips</span></a>
                                                <span class="tptn_date"> September 3, 2014</span> 
                                            </span>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </aside>
                            
                        </div> -->
                        
                    </div>
                    
                </div>
                <!-- widget-area -->
                
               
            
            </div>
            <!-- layout-medium -->
                
                
            <!-- .site-info -->
            <div class="site-info">
            	
                <!-- layout-medium -->
            	<div class="layout-medium">
                
            		<div class="textwidget">mit <i class="pw-icon-heart"> programmiert</i> <em>von dem</em> X-Mas Team <?php echo date('Y'); ?></div>
                
                </div>
            	<!-- layout-medium -->
            
            </div>
            <!-- .site-info -->
            
            
            
		</footer>
        <!-- site-footer -->

        
	</div>
    <!-- page -->
    

    <!-- SCRIPTS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script>
	   if (!window.jQuery) { 
		  document.write('<script src="/assets/js/jquery-3.1.1.min.js"><\/script>'); 
	   }
	</script>
    <script src="/assets/js/jquery-migrate-3.0.0.min.js"></script>
    <script src="/assets/js/countdown.js"></script>
    <script src="/assets/js/fastclick.js"></script>
    <script src="/assets/js/jquery.fitvids.js"></script>
    <script src="/assets/js/jquery.viewport.mini.js"></script>
    <script src="/assets/js/jquery.waypoints.min.js"></script>    
    <script src="/assets/js/jquery-validation/jquery.validate.min.js"></script>
    <script src="/assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="/assets/js/jquery.isotope.min.js"></script>
    <script src="/assets/js/jquery.magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="/assets/js/jquery.fluidbox/jquery.fluidbox.min.js"></script>
    <script src="/assets/js/owl-carousel/owl.carousel.min.js"></script>
    <script src="/assets/js/selection-sharer/selection-sharer.js"></script>
    <script src="/assets/js/socialstream.jquery.js"></script>
    <script src="/assets/js/jquery.collagePlus/jquery.collagePlus.min.js"></script>
    <script src="/assets/js/main.js"></script>
    <script src="/assets/js/shortcodes/shortcodes.js"></script>

</body>
</html>
