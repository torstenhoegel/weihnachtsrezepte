<?php include "templates/include/header.php" ?>
        
        <!-- site-main -->
        <div id="main" class="site-main">
            
      
      <div class="layout-medium"> 
                <div id="primary" class="content-area">
                    <!-- site-content -->
                    <div id="content" class="site-content" role="main">
                    
                        <!-- entry-header -->
                        <header class="entry-header">
                          <h2><a href="/">< Zurück</a></h2>
                          <h1 class="entry-title"><?php echo htmlspecialchars( $results['pageHeading'] ) ?></h1>
                          <?php if ( $results['category'] ) { ?>
                                <h3 class="categoryDescription"><?php echo $results['category']->description ?></h3>
                          <?php } ?>
                        </header>
                        <!-- entry-header -->
                        
                        
                       
                        
                
                
                        <!-- BLOG LIST -->
                        <div class="blog-list  blog-stream">
                        
                
                        
                        <?php foreach ( $results['articles'] as $article ) { ?>
                        
                          <!-- .hentry -->
                            <article class="hentry post has-post-thumbnail">
                                <!-- .featured-image -->
                                <div class="featured-image">
                                    <a href="/lied/<?php echo $article->id?>"><img src="<?php echo $article->img?>" alt="IMG"></a>
                                </div>
                                <!-- .featured-image -->
                                
                                <!-- .hentry-middle -->
                                <div class="hentry-middle">
                                    <!-- .entry-header -->
                                    <header class="entry-header">
                                      <?php if ( !$results['category'] && $article->categoryId ) { ?>
                                        <!-- .entry-meta -->
                                        <div class="entry-meta">
                                            <span class="cat-links">
                                                <a href="/lied/<?php echo $article->id?>" title="" rel="category tag"><?php echo htmlspecialchars( $results['categories'][$article->categoryId]->name ) ?></a>
                                            </span> 
                                        </div>
                                        <!-- .entry-meta -->
                                      <?php } ?> 
                                        <!-- .entry-title -->
                                        <h2 class="entry-title"><a href="/lied/<?php echo $article->id?>"><?php echo htmlspecialchars( $article->title )?></a></h2>
                                    </header>
                                    <!-- .entry-header -->
                                    
                                    <!-- .entry-content -->
                                    <div class="entry-content">        
                                        <p><?php echo $article->summary?>
                                            <span class="more">
                                                <a href="/lied/<?php echo $article->id?>" class="more-link">Mehr</a>
                                            </span>
                                        </p>
                                    </div>
                                    <!-- .entry-content -->
                                </div>
                                <!-- .hentry-middle -->
                            </article>
                            <!-- .hentry -->
                            <?php } ?>

                            
                       </div>
                       <!-- BLOG LIST -->
                        
                        
                        
                    </div>
                    <!-- site-content -->
            
                </div>
                <!-- primary -->    
            
            
              
            
            
            </div>
            <!-- layout -->
        
        
        </div>
        <!-- site-main -->
        
<?php include "templates/include/footer.php" ?>