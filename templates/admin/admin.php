<!-- INCLUDING THE HEADER OF THE ADMIN DASHBOARD -->
<?php include "templates/include/header.php" ?>

        <!-- site-main -->
        <div id="main" class="site-main">
            <div class="layout-medium">
                <div id="primary" class="content-area">

                    <!-- site-content -->
                    <div id="content" class="site-content" role="main"> <!-- .hentry -->
                        <article class="hentry page">
                        
                          
                            <!-- .entry-navigation -->
                            <header class="entry-header">

                                <h1 class="entry-title"><strong>Willkommen zurück <?php echo htmlspecialchars( $_SESSION['username']) ?> <?php
                                  if ($_SESSION['username']=="lea"){
                                    echo "💌";
                                  }
                                ?>!</strong></h1><br>
                                <h2 class="entry-title">> Was möchtest du heute tun?</h1><br><br>

                                <h2><a href="admin.php?action=listPages">Seiten Bearbeiten</a></h2>
                                <h2><a href="admin.php?action=listCategories">Kategorien Bearbeiten</a></h2>
                                <h2><a href="admin.php?action=listArticles">Lieder Bearbeiten</a></h2><br><br>

                                <h1 class="entry-title"><a href="admin.php?action=logout"?>Abmelden</a></h1>



                            </header>
                            <!-- .entry-header -->   
                            
                            
                        </article>
                        <!-- .hentry -->
                  
                    
                  </div>
                    <!-- site-content -->
            
            </div>
                <!-- primary -->    
            

            </div>
            <!-- layout -->
        
        
        </div>
        <!-- site-main -->

<!-- INCLUDING THE FOOTER OF THE ADMIN DASHBOARD -->
<?php include "templates/include/footer.php" ?>