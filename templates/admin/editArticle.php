<!-- INCLUDING THE HEADER -->
<?php include "templates/include/header.php" ?>


 <section class="flat-dividers">
      <div class="container">
        <div class="row">
          <div class="col-md-12">

              <!-- INCLUDING THE WYSIWYG IF ARTICLE EXISTS -->
                <?php if ( $results['article']->id ) { ?>
                  <script>
                  tinymce.init({
                    selector: "textarea",  // change this value according to your HTML
                    plugins: "code",
                    //toolbar: "code",
                    menubar: "tools"
                  });
                  </script>
                 <?php } ?>

     

      <div id="adminHeader">
        <h4>Hallo <?php echo htmlspecialchars( $_SESSION['username'])?>, <a href="admin.php?action=logout"?>Abmelden</a></h4>
      </div>

      <h1><?php echo $results['pageTitle']?></h1>

      <form action="admin.php?action=<?php echo $results['formAction']?>" id="autosave" method="post">
        <input type="hidden" name="articleId" value="<?php echo $results['article']->id ?>"/>

        <?php if ( isset( $results['errorMessage'] ) ) { ?>
                <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
        <?php } ?>

        <ul>

          <br>

          <li>
            <label for="title">Artikel Titel</label>
            <input type="text" name="title" id="title" placeholder="Name des Songs" required autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['article']->title )?>" />
          </li>

          <br>

          <li>
            <label for="summary">Artikel Zusammenfassung</label>
            <textarea name="summary" id="summary" placeholder="Kurzbeschreibung des Songs" required maxlength="1000" style="height: 5em;"><?php echo htmlspecialchars( $results['article']->summary )?></textarea>
          </li>

          <br>

          <li>
            <label for="content">Artikel Inhalt</label>
            <textarea name="content" id="content" placeholder="Der HTML-Inhalt des Songs" required maxlength="100000" style="height: 30em;"><?php echo htmlspecialchars( $results['article']->content )?></textarea>
          </li>

          <br>

          <li>
            <label for="category">Kategorie</label>
            <select name="categoryId">
              <option value="0"<?php echo !$result['article']->categoryId ? " selected" : ""?>>(keine)</option>
              <?php foreach ( $results['categories'] as $category ) { ?>
              <option value="<?php echo $category->id?>"<?php echo ( $category->id == $results['article']->categoryId ) ? " selected" : ""?>><?php echo htmlspecialchars( $category->name )?></option>
              <?php } ?>
            </select>
          </li>

          <br>

          <li>
            <label for="img">Artikel Bild</label>
            <input type="text" name="img" id="img" placeholder="Bild URL" required autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['article']->img )?>" />
          </li>

        </ul>
        
        <br><br>
        <div class="buttons">
          <input type="submit" name="saveChanges" id="saveChanges" value="Änderungen speichern" />
          <input type="submit" formnovalidate name="cancel" value="Abbrechen" />
        </div>

      </form>

<?php if ( $results['article']->id ) { ?>
      <p><a href="admin.php?action=deleteArticle&amp;articleId=<?php echo $results['article']->id ?>" onclick="return confirm('Möchten Sie diesen Artikel löschen?')">Artikel löschen</a></p>
<?php } ?>

</div>
        </div>
      </div>
    </section>

<!-- INCLUDING THE FOOTER -->
<?php include "templates/include/footer.php" ?>

