// @todo

<?php include "templates/include/header.php" ?>
    <!-- site-main -->
    <div id="main" class="site-main">
        <div class="layout-medium">
            <div id="primary" class="content-area">

                <!-- site-content -->
                <div id="content" class="site-content" role="main"> <!-- .hentry -->
                    <article class="hentry page">


                        <!-- .entry-header -->
                        <header class="entry-header">
                            <h1><a href="/admin.php">< Zurück</a></h1><br>
                            <h1 class="entry-title"><strong><u>Seiten</u></strong></h1>
                            <h2><a href="admin.php?action=newPage">Neu erstellen</a></h4>
                        </header>
                        <!-- .entry-header -->


                        <!-- .entry-content -->
                        <div class="entry-content">

                            <?php if ( isset( $results['errorMessage'] ) ) { ?>
                                <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
                            <?php } ?>
                            <?php if ( isset( $results['statusMessage'] ) ) { ?>
                                <div class="statusMessage"><?php echo $results['statusMessage'] ?></div>
                            <?php } ?>

                            <hr>
                            <?php foreach ( $results['seiten'] as $seiten ) { ?>

                                <h2><a href="admin.php?action=editPage&amp;pageId=<?php echo $seiten->id?>"><?php echo $seiten->name?></a></h2>
                                <p>LINK: <a target="_blank" href="/seite/<?php echo $seiten->id?>">/seite/<?php echo $seiten->id?></a></p>
                                <hr>
                            <?php } ?>

                            <br><br>

                            <p><?php echo $results['totalRows']?> seite<?php echo ( $results['totalRows'] != 1 ) ? 'n' : '' ?> insgesamt.</p>





                        </div>
                        <!-- .entry-content -->


                    </article>
                    <!-- .hentry -->


                </div>
                <!-- site-content -->

            </div>
            <!-- primary -->





        </div>
        <!-- layout -->


    </div>
    <!-- site-main -->

<?php include "templates/include/footer.php" ?>