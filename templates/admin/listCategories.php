<?php include "templates/include/header.php" ?>
        <!-- site-main -->
        <div id="main" class="site-main">
<div class="layout-medium"> 
            <div id="primary" class="content-area">
             
            
            
            
             
        
          

            
                    <!-- site-content -->
                    <div id="content" class="site-content" role="main"> <!-- .hentry -->
                        <article class="hentry page">
                        
                          
                            <!-- .entry-header -->   
                            <header class="entry-header">
                                <h1><a href="/admin.php">< Zurück</a></h1><br>
                                <h1 class="entry-title"><strong><u>Kategorien</u></strong></h1>
                                <h2><a href="admin.php?action=newCategory"?>Neu erstellen</a></h4>
                            </header>
                            <!-- .entry-header -->   
                            
                            
                            <!-- .entry-content -->
                            <div class="entry-content">
                              
                              <?php if ( isset( $results['errorMessage'] ) ) { ?>
                                      <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
                              <?php } ?>
                              <?php if ( isset( $results['statusMessage'] ) ) { ?>
                                      <div class="statusMessage"><?php echo $results['statusMessage'] ?></div>
                              <?php } ?>
                          
                              <hr>
                             <?php foreach ( $results['categories'] as $category ) { ?>
                                
                                <p><a href="admin.php?action=editCategory&amp;categoryId=<?php echo $category->id?>"><?php echo $category->name?></a></p>
                                <p>Link: <a target="_blank" href="/kategorie/<?php echo $category->id?>">/kategorie/<?php echo $category->id?></a></p>
                                <hr>
                              <?php } ?>

                              <br><br>

                             <p><?php echo $results['totalRows']?> categor<?php echo ( $results['totalRows'] != 1 ) ? 'ies' : 'y' ?> in total.</p>
                                
                    
                                
                                
                               
                            </div>
                            <!-- .entry-content -->
                            
                            
                        </article>
                        <!-- .hentry -->
                  
                    
                  </div>
                    <!-- site-content -->
            
            </div>
                <!-- primary -->    
            
            
              
            
            
            </div>
            <!-- layout -->
        
        
        </div>
        <!-- site-main -->

<?php include "templates/include/footer.php" ?>