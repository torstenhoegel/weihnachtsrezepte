<?php include "templates/include/header.php" ?>
        <!-- site-main -->
        <div id="main" class="site-main">
<div class="layout-medium"> 
            <div id="primary" class="content-area">
             
            
            
            
             
        
          

            
                    <!-- site-content -->
                    <div id="content" class="site-content" role="main"> <!-- .hentry -->
                        <article class="hentry page">
                        
                          
                            <!-- .entry-header -->   
                            <header class="entry-header">
                                <h1 class="entry-title">Anmelden</h1>
                            </header>
                            <!-- .entry-header -->   
                            
                            
                            <!-- .entry-content -->
                            <div class="entry-content">
                              
                              <p>Bitte melden Sie sich an, um fortzufahren.</p>
                              <?php if ( isset( $results['errorMessage'] ) ) { ?>
                                      <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
                              <?php } ?>
                          
                            
                              <form action="admin.php?action=login" method="post" accept-charset="utf-8" class="validate-form">
                                <input type="hidden" name="login" value="true" />
                                <p>
                                  <label for="your-name">Benutzername *</label>
                                  <input type="text" name="username" id="username" class="required">
                                </p>
                                
                                <p>
                                  <label for="password">Passwort *</label>
                                  <input type="password" name="password" id="password" class="required">
                                </p>
                                
                                <p>
                                  <input type="submit" value="Anmelden">
                                </p>
                              </form>
                                
                                
                                
                                
                               
                            </div>
                            <!-- .entry-content -->
                            
                            
                        </article>
                        <!-- .hentry -->
                  
                    
                  </div>
                    <!-- site-content -->
            
            </div>
                <!-- primary -->    
            
            
              
            
            
            </div>
            <!-- layout -->
        
        
        </div>
        <!-- site-main -->

<?php include "templates/include/footer.php" ?>