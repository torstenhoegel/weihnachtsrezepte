<?php include "templates/include/header.php" ?>


 <section class="flat-dividers">
      <div class="container">
        <div class="row">
          <div class="col-md-12">

            <?php if ( $results['category']->id ) { ?>
              <script>
                  tinymce.init({
                    selector: "textarea",  // change this value according to your HTML
                    plugins: "code",
                    //toolbar: "code",
                    menubar: "tools"
                  });
                  </script>

            <?php } ?>
     

      <div id="adminHeader">
        <h4>Hallo <?php echo htmlspecialchars( $_SESSION['username'])?>, <a href="admin.php?action=logout"?>Abmelden</a></h4>
      </div>

      <h1><?php echo $results['pageTitle']?></h1>

      <form action="admin.php?action=<?php echo $results['formAction']?>" method="post">
        <input type="hidden" name="categoryId" value="<?php echo $results['category']->id ?>"/>

        <?php if ( isset( $results['errorMessage'] ) ) { ?>
                <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
        <?php } ?>

        <ul>

          <br>

          <li>
            <label for="name">Category Name</label>
            <input type="text" name="name" id="name" placeholder="Name of the category" required autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['category']->name )?>" />
          </li>

          <br>

          <li>
            <label for="description">Description</label>
            <textarea name="description" id="description" placeholder="Brief description of the category" required maxlength="1000" style="height: 5em;"><?php echo htmlspecialchars( $results['category']->description )?></textarea>
          </li>

          <br>

          <li>
            <label for="description">Bild</label>
            <input type="text" name="img" id="img" placeholder="Link zum Bild" required autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['category']->img )?>" />
          </li>



        </ul>
        
        <br><br>
        <div class="buttons">
          <input type="submit" name="saveChanges" value="Änderungen speichern" />
          <input type="submit" formnovalidate name="cancel" value="Abbrechen" />
        </div>

      </form>

<?php if ( $results['category']->id ) { ?>
      <p><a href="admin.php?action=deleteCategory&amp;categoryId=<?php echo $results['category']->id ?>" onclick="return confirm('Delete This Category?')">Delete This Category</a></p>
<?php } ?>

</div>
        </div>
      </div>
    </section>

<?php include "templates/include/footer.php" ?>

