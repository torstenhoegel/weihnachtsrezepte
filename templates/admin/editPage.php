<?php include "templates/include/header.php" ?>


 <section class="flat-dividers">
      <div class="container">
        <div class="row">
          <div class="col-md-12">

<?php if ( $results['seiten']->id ) { ?>
  
  <script>
      tinymce.init({
        selector: "textarea",  // change this value according to your HTML
        plugins: "code",
        //toolbar: "code",
        menubar: "tools"
      });
      </script>

<?php } ?>
     

      <div id="adminHeader">
        <h4>Hallo <?php echo htmlspecialchars( $_SESSION['username'])?>, <a href="admin.php?action=logout"?>Abmelden</a></h4>
      </div>

      <h1><?php echo $results['pageTitle']?></h1>

      <form action="admin.php?action=<?php echo $results['formAction']?>" method="post">
        <input type="hidden" name="pageId" value="<?php echo $results['seiten']->id ?>"/>

        <?php if ( isset( $results['errorMessage'] ) ) { ?>
                <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
        <?php } ?>

        <ul>

          <br>

          <li>
            <label for="name">Seiten Name</label>
            <input type="text" name="name" id="name" placeholder="Name der Seite" required autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['seiten']->name )?>" />
          </li>

          <br>

          <li>
            <label for="inhalt">Description</label>
            <textarea type="text" name="inhalt" id="inhalt" placeholder="Seiten Inhalt" required maxlength="1000" style="height: 5em;"><?php echo htmlspecialchars( $results['seiten']->inhalt )?></textarea>
          </li>

          <br>




        </ul>
        
        <br><br>
        <div class="buttons">
          <input type="submit" name="saveChanges" value="Änderungen speichern" />
          <input type="submit" formnovalidate name="cancel" value="Abbrechen" />
        </div>

      </form>

<?php if ( $results['seiten']->id ) { ?>
      <p><a href="admin.php?action=deletePage&amp;pageId=<?php echo $results['seiten']->id ?>" onclick="return confirm('Diese Seite Löschen?')">Seite Löschen</a></p>
<?php } ?>

</div>
        </div>
      </div>
    </section>

<?php include "templates/include/footer.php" ?>

