

<?php include "templates/include/header.php" ?>

        <!-- site-main -->
        <div id="main" class="site-main"> <!-- .featured-top -->
            <div class="featured-top">
                    
                <img src="<?php echo $results['article']->img?>" alt="post-image">
            	
                <!-- .post-thumbnail -->
                <div class="post-thumbnail" style="background-image:url(<?php echo $results['article']->img?>)">
                                          
                    <!-- .entry-header -->
                    <header class="entry-header">
                        
                                        
                        <!-- .entry-meta -->
                        <div class="entry-meta">
                            <span class="cat-links">
                                <a href="/kategorie/<?php echo $results['category']->id?>" title="View all posts in <?php echo $results['article']->category?>" rel="category tag">Kategorie <?php echo htmlspecialchars( $results['category']->name ) ?></a>
                            </span>	
                        </div>
                        <!-- .entry-meta -->
                        
                        <!-- .entry-title -->
                        <h1 class="entry-title"><?php echo htmlspecialchars( $results['article']->title )?></h1>
                                      
                        
                    </header>
                    <!-- .entry-header -->
                    
                </div>
            	<!-- .post-thumbnail -->
                
            </div>
            <!-- .featured-top -->	
            
			
			<div class="layout-medium"> 
                <div id="primary" class="content-area">
             
            
            
            
                    <!-- site-content -->
                    <div id="content" class="site-content" role="main"> <!-- .hentry -->
                            <article class="hentry post single-post">
                            
                            	
                                <!-- .entry-content -->
                                <div class="entry-content">
                                                                        
                                    <p><?php echo $results['article']->content?></p>
                                    
                                </div>
                                <!-- .entry-content -->
                                
                                
                                     
                            </article>
                            <!-- .hentry -->



                            <?php include "templates/include/footer.php" ?>