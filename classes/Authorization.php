<?php

/**
 * Class to handle article categories
 */

class Auth
{
  // Properties

  /**
  * @var int The page ID from the database
  */
  public $id = null;

  /**
  * @var string Name of the page
  */
  public $username = null;

  /**
  * @var string The Content of the page
  */
  public $password = null;


  /**
  * Sets the object's properties using the values in the supplied array
  *
  * @param assoc The property values
  */

  public function __construct( $data=array() ) {
    if ( isset( $data['id'] ) ) $this->id = (int) $data['id'];
    if ( isset( $data['username'] ) ) $this->username = $data['username'];
    if ( isset( $data['password'] ) ) $this->password = $data['password'];
  }


  /**
  * Sets the object's properties using the edit form post values in the supplied array
  *
  * @param assoc The form post values
  */

  public function storeFormValues ( $params ) {

    // Store all the parameters
    $this->__construct( $params );
  }





  public function data( $username ) {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "SELECT * FROM accounts WHERE username = ':username'";
    $st = $conn->prepare( $sql );
    $st->bindValue( ":username", $username, PDO::PARAM_INT );
    $st->execute();
    $row = $st->fetch(); 
    echo $row;
    $conn = null;
  }

  

}

?>
