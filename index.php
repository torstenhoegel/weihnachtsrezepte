<?php

require( "config.php" );
$action = isset( $_GET['action'] ) ? $_GET['action'] : "";

switch ( $action ) {
  case 'archive':
    archive();
    break;
  case 'viewArticle':
    viewArticle();
    break;
  case 'viewPage':
    viewPage();
    break;
  case 'category':
    category();
    break;
  default:
    homepage();
}

function archive() {
  $results = array();
  $categoryId = ( isset( $_GET['categoryId'] ) && $_GET['categoryId'] ) ? (int)$_GET['categoryId'] : null;
  $results['category'] = Category::getById( $categoryId );
  $data = Article::getList( 100000, $results['category'] ? $results['category']->id : null );
  $results['articles'] = $data['results'];
  $results['totalRows'] = $data['totalRows'];
  $data = Category::getList();
  $results['categories'] = array();
  foreach ( $data['results'] as $category ) $results['categories'][$category->id] = $category;
  $results['pageHeading'] = $results['category'] ?  $results['category']->name : "Lieder";
  $results['pageTitle'] = $results['pageHeading'] . " | XMas-Songs";
  require( TEMPLATE_PATH . "/archive.php" );
}

function viewArticle() {
  if ( !isset($_GET["articleId"]) || !$_GET["articleId"] ) {
    homepage();
    return;
  }

  $results = array();
  $results['article'] = Article::getById( (int)$_GET["articleId"] );
  $data = Category::getList();
  foreach ( $data['results'] as $category ) $results['categories'][$category->id] = $category;
  $results['category'] = Category::getById( $results['article']->categoryId );
  $results['pageTitle'] = $results['article']->title . " | XMas-Songs";
  require( TEMPLATE_PATH . "/viewArticle.php" );
}

// function category() {
//   $results = array();
//   $link = $_GET["category"];
//   $data = Article::getCategory($link);
//   //$results['articles'] = $data['totalRows'];
//   $results['articles'] = $data['results'];
//   $results['totalRows'] = $data['totalRows'];
//   $results['pageTitle'] = $_GET["category"] . " | XMas-Songs";
//   require( TEMPLATE_PATH . "/category.php" );
// }

// function homepage() {
//   $results = array();
//   $data = Article::getList( HOMEPAGE_NUM_ARTICLES );
//   $results['articles'] = $data['results'];
//   $results['totalRows'] = $data['totalRows'];
//   $results['pageTitle'] = "XMas-Songs";
//   require( TEMPLATE_PATH . "/homepage.php" );
// }

function homepage() {
  $results = array();
  $data = Article::getList( HOMEPAGE_NUM_ARTICLES );
  $results['articles'] = $data['results'];
  $results['totalRows'] = $data['totalRows'];
  $data = Category::getList();
  $results['categories'] = array();
  foreach ( $data['results'] as $category ) $results['categories'][$category->id] = $category; 
  $results['pageTitle'] = "XMas-Songs";
  require( TEMPLATE_PATH . "/homepage.php" );
}

function viewPage() {
  if ( !isset($_GET["pageId"]) || !$_GET["pageId"] ) {
    homepage();
    return;
  }

  $results = array();
  $results['seiten'] = Seiten::getById( (int)$_GET["pageId"] );
  $data = Category::getList();
  foreach ( $data['results'] as $category ) $results['categories'][$category->id] = $category;
  // $results['category'] = Category::getById( $results['article']->categoryId );
  $results['pageTitle'] = $results['seiten']->name . " | XMas-Songs";
  require( TEMPLATE_PATH . "/viewPage.php" );
}

?>