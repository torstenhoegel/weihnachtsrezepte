Build by Torsten H�gel <torsten.hoegel@go-on-net.de> Go-On Network UG (haftungsbeschr�nkt)

working example: https://xmas.go-on-net.de/

# XMas Songs
## HTML Website built on a custom build PHP Framework

The whole Website is built on classes, routers and controllers.

## Routing the traffic

Starting with the .htaccess it routes the traffic going to the website into

index.php // Frontend
&
admin.php // Backend

This routes the ```requests``` into the right file with the defined "cases".

The frontend routing part does also rewrite traffic onto the index.php with rewrite conditions to improve the SEO performance and readability of the URL.
e.g.:

```
RewriteRule ^lied/([^/]+)/?$ index.php?action=viewArticle&articleId=$1 [L]
RewriteRule ^kategorie/([^/]+)/?$ index.php?action=archive&categoryId=$1 [L]
RewriteRule ^seite/([^/]+)/?$ index.php?action=viewPage&pageId=$1 [L]
```

## Controllers

The files index.php and admin.php include all cases where the requests are being routed.

e.g.:

```admin.php?action=listPagesadmin.php?action=listPages```

where as "listPages" is the action the controller reads of the current url.

This refers to the functions like newArticle() where we define the title of the page and listen for submit actions like $_POST from formulars.
These actions refer to public functions found in the classes.

## Classes

Our classes define actions for:

```Article.php``` -> Songs and their Attributes

```Category.php``` -> Categories and their Attributes

```Seiten.php``` -> Pages and their Attributes


All classes include constructors, storeFormValues functions, and public functions like getById().

At this point of time we do connect to our MYSQL backend Server hosted at "server.hosting-go-on.de" and refers to the tables.

## Templates

The /tempates folder does include the HTML templates separated into:

```/includes``` - which are the same on all pages e.g. header and footer

```/admin``` - all admin pages templates for the backend


The template files do include a lot of php like ```foreach()``` loops to read the data of the json [] formatted $results data.
This allows us to use shortcodes to show information like:

```<?php echo $article->id?> ```

## Assets

The /assets folder includes all html files, including subfolders like /css or /js.

Some content like images for ```articles``` or ```categories``` which are loaded from URLs are saved on MYSQL tables and are simply included in the ```src``` attribute of ```<img>``` tags or ```href``` attribute ```<a>``` tags.

## Extras

### E-Mail Newsletter
The E-Mail Newsletter is built with the Go-On Network UG E-Mail Marketing Application.

### Login
The Login functionality is simply defined in the config.php where username and password are asked to be matching.