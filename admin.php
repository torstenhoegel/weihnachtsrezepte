<?php

require( "config.php" );
session_start();
$action = isset( $_GET['action'] ) ? $_GET['action'] : "";
$username = isset( $_SESSION['username'] ) ? $_SESSION['username'] : "";

if ( $action != "login" && $action != "logout" && !$username ) {
  login();
  exit;
}

switch ( $action ) {
  case 'login':
    login();
    break;
  case 'logout':
    logout();
    break;
  case 'listArticles':
    listArticles();
    break;
  case 'newArticle':
    newArticle();
    break;
  case 'editArticle':
    editArticle();
    break;
  case 'deleteArticle':
    deleteArticle();
    break;
  case 'listCategories':
    listCategories();
    break;
  case 'newCategory':
    newCategory();
    break;
  case 'editCategory':
    editCategory();
    break;
  case 'deleteCategory':
    deleteCategory();
    break;
  case 'listPages':
    listPages();
    break;
  case 'newPage':
    newPage();
    break;
  case 'editPage':
    editPage();
    break;
  case 'deletePage':
    deletePage();
    break;
  default:
    defaultf();
}





//AUTH @todo WORK IN PROGRESS - USER-AUTH

function verify() {

    $username = "";
    $password = "";

    if ( in_array($username)) {}

}


function login() {

  $results = array();
  $results['pageTitle'] = "Login | XMas-Songs";


  if ( isset( $_POST['login'] ) ) {

    // User has posted the login form: attempt to log the user in

    if ( $_POST['username'] == ADMIN_USERNAME && $_POST['password'] == ADMIN_PASSWORD || $_POST['username'] == ADMIN_USERNAME_2 && $_POST['password'] == ADMIN_PASSWORD_2 || $_POST['username'] == ADMIN_USERNAME_3 && $_POST['password'] == ADMIN_PASSWORD_3 ) {

      // Login successful: Create a session and redirect to the admin homepage
      $_SESSION['username'] = $_POST['username'];
      header( "Location: admin.php" );

    } else {

      // Login failed: display an error message to the user
      $results['errorMessage'] = "Falscher Benutzername oder falsches Passwort. Bitte versuchen Sie es erneut.";
      require( TEMPLATE_PATH . "/admin/loginForm.php" );
    }

  } else {

    // User has not posted the login form yet: display the form
    require( TEMPLATE_PATH . "/admin/loginForm.php" );
  }

}




// function login() {

//   $results = array();
//   $results['pageTitle'] = "Login | XMas-Songs";

//   if ( isset( $_POST['login'] ) ) {


//     $results = array();
//     $results['user'] = Auth::data( $_POST['username'] );

//     print_r($results['user']);

//     if ($results['password'] == $_POST['password']) {

//       // Login successful: Create a session and redirect to the admin homepage
//       $_SESSION['username'] = $_POST['username'];
//       header( "Location: admin.php" );

//     }

//     else {
//       // Login failed: display an error message to the user
//       $results['errorMessage'] = "Falscher Benutzername oder falsches Passwort. Bitte versuchen Sie es erneut.";
//       require( TEMPLATE_PATH . "/admin/loginForm.php" );
//     }

//   } else {

//     // User has not posted the login form yet: display the form
//     require( TEMPLATE_PATH . "/admin/loginForm.php" );
//   }

// }









function logout() {
  unset( $_SESSION['username'] );
  header( "Location: admin.php" );
}




// DEFAULT FUNCTIONS


function defaultf() {

  $results['pageTitle'] = "Administration";

    // User has not posted the article edit form yet: display the form
  $data = Category::getList();
  $results['categories'] = $data['results'];
  require( TEMPLATE_PATH . "/admin/admin.php" );

}







// ARTIKEL FUNCTIONS


function newArticle() {

  $results = array();
  $results['pageTitle'] = "Neues Lied";
  $results['formAction'] = "newArticle";

  if ( isset( $_POST['saveChanges'] ) ) {

    // User has posted the article edit form: save the new article
    $article = new Article;
    $article->storeFormValues( $_POST );
    $article->insert();
    header( "Location: admin.php?action=listArticles&status=changesSaved" );

  } elseif ( isset( $_POST['cancel'] ) ) {

    // User has cancelled their edits: return to the article list
    header( "Location: admin.php?action=listArticles" );
  } else {

    // User has not posted the article edit form yet: display the form
    $results['article'] = new Article;
    $data = Category::getList();
    $results['categories'] = $data['results'];
    require( TEMPLATE_PATH . "/admin/editArticle.php" );
  }

}


function editArticle() {

  $results = array();
  $results['pageTitle'] = "Lied bearbeiten";
  $results['formAction'] = "editArticle";

  if ( isset( $_POST['saveChanges'] ) ) {

    // User has posted the article edit form: save the article changes

    if ( !$article = Article::getById( (int)$_POST['articleId'] ) ) {
      header( "Location: admin.php?error=articleNotFound" );
      return;
    }

    $article->storeFormValues( $_POST );
    $article->update();
    header( "Location: admin.php?action=listArticles&status=changesSaved" );

  } elseif ( isset( $_POST['cancel'] ) ) {

    // User has cancelled their edits: return to the article list
    header( "Location: admin.php?action=listArticles" );
  } else {

    // User has not posted the article edit form yet: display the form
    $results['article'] = Article::getById( (int)$_GET['articleId'] );
    $data = Category::getList();
    $results['categories'] = $data['results'];
    require( TEMPLATE_PATH . "/admin/editArticle.php" );
  }

}


function deleteArticle() {

  if ( !$article = Article::getById( (int)$_GET['articleId'] ) ) {
    header( "Location: admin.php?action=listArticles&error=articleNotFound" );
    return;
  }

  $article->delete();
  header( "Location: admin.php?action=listArticles&status=articleDeleted" );
}


function listArticles() {
  $results = array();
  $data = Article::getList();
  $results['articles'] = $data['results'];
  $results['totalRows'] = $data['totalRows'];
  $data = Category::getList();
  $results['categories'] = array();
  foreach ( $data['results'] as $category ) $results['categories'][$category->id] = $category;
  $results['pageTitle'] = "Alle Lieder";

  if ( isset( $_GET['error'] ) ) {
    if ( $_GET['error'] == "articleNotFound" ) $results['errorMessage'] = "Fehler: Lied nicht gefunden.";
  }

  if ( isset( $_GET['status'] ) ) {
    if ( $_GET['status'] == "changesSaved" ) $results['statusMessage'] = "Deine Änderungen wurden gespeichert.";
    if ( $_GET['status'] == "articleDeleted" ) $results['statusMessage'] = "Lied gelöscht.";
  }

  require( TEMPLATE_PATH . "/admin/listArticles.php" );
}











// KATEGORIE FUNKTION


function listCategories() {
  $results = array();
  $data = Category::getList();
  $results['categories'] = $data['results'];
  $results['totalRows'] = $data['totalRows'];
  $results['pageTitle'] = "Lied Kategorien";

  if ( isset( $_GET['error'] ) ) {
    if ( $_GET['error'] == "categoryNotFound" ) $results['errorMessage'] = "Fehler: Kategorie nicht gefunden.";
    if ( $_GET['error'] == "categoryContainsArticles" ) $results['errorMessage'] = "Fehler: Die Kategorie enthält Lieder. Lösche die Lieder oder ordne sie einer anderen Kategorie zu, bevor diese Kategorie gelöscht werden kann.";
  }

  if ( isset( $_GET['status'] ) ) {
    if ( $_GET['status'] == "changesSaved" ) $results['statusMessage'] = "Deine Änderungen wurden gespeichert.";
    if ( $_GET['status'] == "categoryDeleted" ) $results['statusMessage'] = "Kategorie gelöscht.";
  }

  require( TEMPLATE_PATH . "/admin/listCategories.php" );
}


function newCategory() {

  $results = array();
  $results['pageTitle'] = "Neue Kategorie";
  $results['formAction'] = "newCategory";

  if ( isset( $_POST['saveChanges'] ) ) {

    // User has posted the category edit form: save the new category
    $category = new Category;
    $category->storeFormValues( $_POST );
    $category->insert();
    header( "Location: admin.php?action=listCategories&status=changesSaved" );

  } elseif ( isset( $_POST['cancel'] ) ) {

    // User has cancelled their edits: return to the category list
    header( "Location: admin.php?action=listCategories" );
  } else {

    // User has not posted the category edit form yet: display the form
    $results['category'] = new Category;
    require( TEMPLATE_PATH . "/admin/editCategory.php" );
  }

}


function editCategory() {

  $results = array();
  $results['pageTitle'] = "Editiere Kategorie";
  $results['formAction'] = "editCategory";
  $data = Category::getList();
  foreach ( $data['results'] as $category ) $results['categories'][$category->id] = $category;

  if ( isset( $_POST['saveChanges'] ) ) {

    // User has posted the category edit form: save the category changes

    if ( !$category = Category::getById( (int)$_POST['categoryId'] ) ) {
      header( "Location: admin.php?action=listCategories&error=categoryNotFound" );
      return;
    }

    $category->storeFormValues( $_POST );
    $category->update();
    header( "Location: admin.php?action=listCategories&status=changesSaved" );

  } elseif ( isset( $_POST['cancel'] ) ) {

    // User has cancelled their edits: return to the category list
    header( "Location: admin.php?action=listCategories" );
  } else {

    // User has not posted the category edit form yet: display the form
    $results['category'] = Category::getById( (int)$_GET['categoryId'] );
    require( TEMPLATE_PATH . "/admin/editCategory.php" );
  }

}


function deleteCategory() {

  if ( !$category = Category::getById( (int)$_GET['categoryId'] ) ) {
    header( "Location: admin.php?action=listCategories&error=categoryNotFound" );
    return;
  }

  $articles = Article::getList( 1000000, $category->id );

  if ( $articles['totalRows'] > 0 ) {
    header( "Location: admin.php?action=listCategories&error=categoryContainsArticles" );
    return;
  }

  $category->delete();
  header( "Location: admin.php?action=listCategories&status=categoryDeleted" );
}










// SEITEN FUNKTIONEN



function listPages() {
  $results = array();
  $data = Seiten::getList();
  $results['seiten'] = $data['results'];
  $results['totalRows'] = $data['totalRows'];
  $results['pageTitle'] = "Seiten";

  if ( isset( $_GET['error'] ) ) {
    if ( $_GET['error'] == "pageNotFound" ) $results['errorMessage'] = "Fehler: Seite nicht gefunden.";
  }

  if ( isset( $_GET['status'] ) ) {
    if ( $_GET['status'] == "changesSaved" ) $results['statusMessage'] = "Deine Änderungen wurden gespeichert.";
    if ( $_GET['status'] == "categoryDeleted" ) $results['statusMessage'] = "Seite gelöscht.";
  }

  require( TEMPLATE_PATH . "/admin/listPages.php" );
}


function newPage() {

  $results = array();
  $results['pageTitle'] = "Neue Seite";
  $results['formAction'] = "newPage";

  if ( isset( $_POST['saveChanges'] ) ) {

    // User has posted the page edit form: save the new page
    $page = new Seiten;
    $page->storeFormValues( $_POST );
    $page->insert();
    header( "Location: admin.php?action=listPages&status=changesSaved" );

  } elseif ( isset( $_POST['cancel'] ) ) {

    // User has cancelled their edits: return to the category list
    header( "Location: admin.php?action=listPages" );
  } else {

    // User has not posted the category edit form yet: display the form
    $results['seiten'] = new Seiten;
    require( TEMPLATE_PATH . "/admin/editPage.php" );
  }

}


function editPage() {

  $results = array();
  $results['pageTitle'] = "Editiere Seite";
  $results['formAction'] = "editPage";
  $data = Seiten::getList();
  foreach ( $data['results'] as $page ) $results['seiten'][$page->id] = $page;

  if ( isset( $_POST['saveChanges'] ) ) {

    // User has posted the category edit form: save the category changes

    if ( !$page = Seiten::getById( (int)$_POST['pageId'] ) ) {
      header( "Location: admin.php?action=listPages&error=pageNotFound" );
      return;
    }

    $page->storeFormValues( $_POST );
    $page->update();
    header( "Location: admin.php?action=listPages&status=changesSaved" );

  } elseif ( isset( $_POST['cancel'] ) ) {

    // User has cancelled their edits: return to the category list
    header( "Location: admin.php?action=listPages" );
  } else {

    // User has not posted the category edit form yet: display the form
    $results['seiten'] = Seiten::getById( (int)$_GET['pageId'] );
    require( TEMPLATE_PATH . "/admin/editPage.php" );
  }

}


function deletePage() {

  if ( !$page = Seiten::getById( (int)$_GET['pageId'] ) ) {
    header( "Location: admin.php?action=listPages&error=pageNotFound" );
    return;
  }

  $page->delete();
  header( "Location: admin.php?action=listPages&status=pageDeleted" );
}

?>